import { WeatherService } from './../weather.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-city-form',
  templateUrl: './city-form.component.html',
  styleUrls: ['./city-form.component.css']
})
export class CityFormComponent implements OnInit {

  constructor(private WeatherService:WeatherService,private router:Router) { }
  name: string;
  

  ngOnInit(): void {
  }

  addCity(){
    this.WeatherService.addCity(this.name).subscribe(
      res => this.router.navigate(['/cities'])
    )
 }
}
