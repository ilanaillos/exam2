import { AuthService } from './../auth.service';
import { WeatherService } from './../weather.service';
import { Component, OnInit } from '@angular/core';
import { City } from '../interfaces/city';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-cities',
  templateUrl: './cities.component.html',
  styleUrls: ['./cities.component.css']
})
export class CitiesComponent implements OnInit {
  cities$:Observable<any>;
  cities:City[] = []
  displayedColumns: string[] = ['name','getData','temperature', 'humidity','wind','image','predict','prediction','delete'];
  weatherData$: any;
  hasError: boolean;
  errorMessage: any;
  city: string;
  result$: any;
  owner: string;

  constructor(private WeatherService:WeatherService, private AuthService:AuthService) { }
  getDataFun(index, name){
    console.log(name);

    
    this.weatherData$=this.WeatherService.searchWeatherData(name).subscribe(
      data => {
        console.log(data);
        this.cities[index].temperature = data.temperature;
        this.cities[index].image = data.image;
        this.cities[index].humidity = data.humidity;
        this.cities[index].wind = data.wind;
        this.city = name;
     
      },
      error=>{
        console.log(error.message);
        this.hasError= true;
        this.errorMessage=error.message;
      }
    )
  }
  predict(index){
  
    this.result$=this.WeatherService.predict(this.cities[index].temperature,this.cities[index].humidity);
    this.result$=this.result$.subscribe(
      result=>{
        console.log(result);
        if (result>50){
          this.cities[index].predict = "It will rain";
        }
        else {
          this.cities[index].predict = "It will not rain";
        }


      }
      
    )
  }

  delete(id){
    console.log(id);
    this.WeatherService.delete(id);
    }

  add(index){
    this.WeatherService.addPrediction(this.cities[index].name,this.cities[index].temperature,this.cities[index].humidity,this.cities[index].wind,this.cities[index].predict,this.owner); 
 
}




  ngOnInit(): void {
    this.AuthService.getUser().subscribe(
      user => {
        this.owner=user.email;
      }
    );
    this.cities$ = this.WeatherService.getCities();
    this.cities$.subscribe(
      docs => {
        this.cities =[];
        for(let document of docs){
          const newCity:City = document.payload.doc.data();
          newCity.id = document.payload.doc.id;
          this.cities.push(newCity);
        }
      }
    )
  }

}
