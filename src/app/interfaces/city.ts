export interface City {
    id?: string,
    name: string,
    temperature: number,
    humidity: number,
    wind: number,
    image: string,
    predict?: string,
    saved?: boolean,
    owner?: string
}
